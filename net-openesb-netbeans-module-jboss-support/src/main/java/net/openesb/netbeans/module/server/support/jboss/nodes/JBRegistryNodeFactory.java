package net.openesb.netbeans.module.server.support.jboss.nodes;

import java.io.File;
import java.io.IOException;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.deploy.spi.DeploymentManager;
import javax.enterprise.deploy.spi.factories.DeploymentFactory;
import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NameNotFoundException;
import javax.naming.NamingException;
import org.netbeans.modules.j2ee.deployment.plugins.api.InstanceProperties;
import org.netbeans.modules.j2ee.jboss4.JBDeploymentFactory;
import org.netbeans.modules.j2ee.jboss4.JBDeploymentManager;
import org.netbeans.modules.j2ee.jboss4.JBoss5ProfileServiceProxy;
import org.netbeans.modules.j2ee.jboss4.ide.ui.JBPluginProperties;
import org.netbeans.modules.j2ee.jboss4.ide.ui.JBPluginUtils;
import org.netbeans.modules.j2ee.jboss4.util.JBProperties;
import org.netbeans.modules.j2ee.sun.api.SimpleNodeExtensionProvider;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author David BRASSELY
 */
public class JBRegistryNodeFactory extends org.netbeans.modules.j2ee.jboss4.nodes.JBRegistryNodeFactory {
    
    private static final Logger LOGGER = Logger.getLogger(JBRegistryNodeFactory.class.getName());
    
    @Override
    public Node getTargetNode(Lookup lookup) {
        Node targetNode = super.getTargetNode(lookup);
        
        targetNode.getChildren().add(getExtensionNodes(lookup));
        
        return targetNode;
    }
    
    private Node[] getExtensionNodes(Lookup lookup) {
        List<Node> nodes = new ArrayList<Node>();
        
        for (SimpleNodeExtensionProvider nep : Lookup.getDefault().lookupAll(SimpleNodeExtensionProvider.class)) {
            if (nep != null) {
                 Node node = nep.getExtensionNode(getConnection(lookup));
                 if (node != null){
                     nodes.add(node);
                 }
             }
        }
        
        return (Node[]) nodes.toArray(new Node[nodes.size()]);
    }
    
    private MBeanServerConnection getConnection(Lookup lookup) {
        JBDeploymentManager dm = (JBDeploymentManager) lookup.lookup(DeploymentManager.class);
        JBDeploymentFactory df = (JBDeploymentFactory) lookup.lookup(DeploymentFactory.class);
        ClassLoader oldLoader = Thread.currentThread().getContextClassLoader();
        InitialContext ctx = null;
        JMXConnector conn = null;

        try {
            InstanceProperties ip = dm.getInstanceProperties();
            JBDeploymentFactory.JBClassLoader loader = df.getJBClassLoader(ip);
            Thread.currentThread().setContextClassLoader(loader);

            JBProperties props = dm.getProperties();
            Properties env = new Properties();

            // Sets the jboss naming environment
            String jnpPort = Integer.toString(
                    JBPluginUtils.getJnpPortNumber(ip.getProperty(JBPluginProperties.PROPERTY_SERVER_DIR)));

            env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.naming.NamingContextFactory");
            env.put(Context.PROVIDER_URL, "jnp://localhost"+ ":"  + jnpPort);
            env.put(Context.OBJECT_FACTORIES, "org.jboss.naming");
            env.put(Context.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces" );
            env.put("jnp.disableDiscovery", Boolean.TRUE);

            final String JAVA_SEC_AUTH_LOGIN_CONF = "java.security.auth.login.config"; // NOI18N
            String oldAuthConf = System.getProperty(JAVA_SEC_AUTH_LOGIN_CONF);

            env.put(Context.SECURITY_PRINCIPAL, props.getUsername());
            env.put(Context.SECURITY_CREDENTIALS, props.getPassword());
            env.put("jmx.remote.credentials", // NOI18N
                    new String[] {props.getUsername(), props.getPassword()});

            File securityConf = new File(props.getRootDir(), "/client/auth.conf");
            if (securityConf.exists()) {
                env.put(Context.INITIAL_CONTEXT_FACTORY, "org.jboss.security.jndi.LoginInitialContextFactory");
                System.setProperty(JAVA_SEC_AUTH_LOGIN_CONF, securityConf.getAbsolutePath()); // NOI18N
            }

            // Gets naming context
            ctx = new InitialContext(env);

            //restore java.security.auth.login.config system property
            if (oldAuthConf != null) {
                System.setProperty(JAVA_SEC_AUTH_LOGIN_CONF, oldAuthConf);
            } else {
                System.clearProperty(JAVA_SEC_AUTH_LOGIN_CONF);
            }

            MBeanServerConnection rmiServer = null;
            try {
                conn = JMXConnectorFactory.connect(new JMXServiceURL(
                        "service:jmx:rmi:///jndi/rmi://localhost:1090/jmxrmi"));

                rmiServer = conn.getMBeanServerConnection();
            } catch (IOException ex) {
                LOGGER.log(Level.FINE, null, ex);
            }

            if (rmiServer == null) {
                // Lookup RMI Adaptor
                rmiServer = (MBeanServerConnection) ctx.lookup("/jmx/invoker/RMIAdaptor"); // NOI18N
            }

            JBoss5ProfileServiceProxy profileService = null;
            try {
                Object service = ctx.lookup("ProfileService"); // NOI18N
                if (service != null) {
                    profileService = new JBoss5ProfileServiceProxy(service);
                }
            } catch (NameNotFoundException ex) {
                LOGGER.log(Level.FINE, null, ex);
            }

            return rmiServer;
        } catch (NameNotFoundException ex) {
            LOGGER.log(Level.FINE, null, ex);
            return null;
        } catch (NamingException ex) {
            LOGGER.log(Level.FINE, null, ex);
            return null;
        } catch (Exception ex) {
            LOGGER.log(Level.FINE, null, ex);
            return null;
        } finally {
            /*
            try {
                if (ctx != null) {
                    ctx.close();
                }
            } catch (NamingException ex) {
                LOGGER.log(Level.FINE, null, ex);
            }
            try {
                if (conn != null) {
                    conn.close();
                }
            } catch (IOException ex) {
                LOGGER.log(Level.FINE, null, ex);
            }*/
            Thread.currentThread().setContextClassLoader(oldLoader);
        }
    }
}
