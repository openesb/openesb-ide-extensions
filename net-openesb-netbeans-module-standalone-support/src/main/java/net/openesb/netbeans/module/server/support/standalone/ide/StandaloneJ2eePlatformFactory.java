package net.openesb.netbeans.module.server.support.standalone.ide;

import java.awt.Image;
import java.io.File;
import java.util.HashSet;
import java.util.Set;
import java.util.WeakHashMap;
import javax.enterprise.deploy.spi.DeploymentManager;
import net.openesb.netbeans.module.server.support.standalone.StandaloneDeploymentManager;
import net.openesb.netbeans.module.server.support.standalone.util.StandaloneProperties;
import org.netbeans.api.java.platform.JavaPlatform;
import org.netbeans.api.java.platform.JavaPlatformManager;
import org.netbeans.modules.j2ee.deployment.plugins.api.InstanceProperties;
import org.netbeans.modules.j2ee.deployment.plugins.spi.J2eePlatformFactory;
import org.netbeans.modules.j2ee.deployment.plugins.spi.J2eePlatformImpl;
import org.netbeans.modules.j2ee.deployment.plugins.spi.J2eePlatformImpl2;
import org.netbeans.spi.project.libraries.LibraryImplementation;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY
 */
public class StandaloneJ2eePlatformFactory extends J2eePlatformFactory {

    private static final WeakHashMap<InstanceProperties,J2eePlatformImplImpl> instanceCache = new WeakHashMap<InstanceProperties,J2eePlatformImplImpl>();
    
    public synchronized J2eePlatformImpl getJ2eePlatformImpl(DeploymentManager dm) {
        assert StandaloneDeploymentManager.class.isAssignableFrom(dm.getClass()) : this + " cannot create platform for unknown deployment manager:" + dm;
        
        return new J2eePlatformImplImpl();
    }
    
    public static class J2eePlatformImplImpl extends J2eePlatformImpl2 {
        
        @Override
        public File getServerHome() {
            return null;
        }

        @Override
        public File getDomainHome() {
            return null;
        }

        @Override
        public File getMiddlewareHome() {
            return null;
        }

        @Override
        public LibraryImplementation[] getLibraries() {
            return new LibraryImplementation[0];
        }

        @Override
        public String getDisplayName() {
            return NbBundle.getMessage(StandaloneJ2eePlatformFactory.class, "MSG_StandaloneServerPlatform");
        }

        @Override
        public Image getIcon() {
            return null;
        }

        @Override
        public File[] getPlatformRoots() {
            return new File[] {getServerHome(), getDomainHome()};
        }

        @Override
        public File[] getToolClasspathEntries(String string) {
            return new File[0];
        }

        @Override
        public boolean isToolSupported(String string) {
            return false;
        }

        @Override
        public Set getSupportedJavaPlatformVersions() {
            Set versions = new HashSet();
            versions.add("1.5"); // NOI18N
            versions.add("1.6"); // NOI18N
            versions.add("1.7"); // NOI18N
            return versions;
        }

        @Override
        public JavaPlatform getJavaPlatform() {
            return JavaPlatformManager.getDefault().getDefaultPlatform();
        }
    }
}
