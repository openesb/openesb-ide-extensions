package net.openesb.netbeans.module.server.support.standalone.nodes;

import javax.swing.Action;
import org.openide.nodes.AbstractNode;
import org.openide.nodes.Children;
import org.openide.nodes.Node;
import org.openide.util.Lookup;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class TargetNode extends AbstractNode {

    public TargetNode(Lookup lookup, Node[] children) {
        super(new Children.Array());
        setDisplayName("target node");
        getChildren().add(children);
    }
    
    @Override
    public Action[] getActions(boolean b) {
        return new Action[] {};
    }
}
