/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance
 * with the License.
 *
 * You can obtain a copy of the License at http://opensource.org/licenses/CDDL-1.0 or
 * http://opensource.org/licenses/cddl1.txt 
 *
 * When distributing Covered Code, include this CDDL Header Notice in each file and
 * include the License file at http://opensource.org/licenses/cddl1.txt . If applicable, add
 * the following below the CDDL Header, with the fields enclosed by brackets []
 * replaced by your own identifying information:
 *
 * "Portions Copyrighted [year] [name of copyright owner]"
 *
 * Contributor(s):
 *
 * Portions Copyrighted 2013 OpenESB Community
 */
package net.openesb.netbeans.module.server.support.standalone.jbi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import org.netbeans.modules.sun.manager.jbi.util.ServerInstance;

/**
 * 
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneServerInstance extends ServerInstance {

    @Override
    public List<File> getClasses() {
        List<File> classes = new ArrayList<File>();
        String serverLocation = this.getUrlLocation();

        String jbi_rt_jar = serverLocation + "/lib/jbi_rt.jar"; // NOI18N

        File jbi_rt_jarFile = new File(jbi_rt_jar);
        if (jbi_rt_jarFile.exists()) {
            classes.add(jbi_rt_jarFile);
        } else {
            throw new RuntimeException("JbiClassLoader: Cannot find "
                    + jbi_rt_jar + ".");
        }
        
        return classes;
    }

    @Override
    public MBeanServerConnection getConnection() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public boolean isJBIEnabled(MBeanServerConnection mBeanServerConnection) {
        try {
            return mBeanServerConnection.isRegistered(new ObjectName(
                    "com.sun.jbi.jse:instance=" + getTarget()));
        } catch (Exception ex) {
            return false;
        }
    }

    @Override
    public boolean isCurrentInstance(MBeanServerConnection mbsc, String instanceHost, boolean isLocalHost) {
        return true;
    }
    
}
