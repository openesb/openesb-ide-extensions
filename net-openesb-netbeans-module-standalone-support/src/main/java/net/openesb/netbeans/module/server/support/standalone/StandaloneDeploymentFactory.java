package net.openesb.netbeans.module.server.support.standalone;

import java.net.URLClassLoader;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.enterprise.deploy.shared.factories.DeploymentFactoryManager;
import javax.enterprise.deploy.spi.DeploymentManager;
import javax.enterprise.deploy.spi.exceptions.DeploymentManagerCreationException;
import javax.enterprise.deploy.spi.factories.DeploymentFactory;
import org.netbeans.modules.j2ee.deployment.plugins.api.InstanceProperties;
import org.openide.util.NbBundle;

/**
 *
 * @author David BRASSELY (brasseld at gmail.com)
 * @author OpenESB Community
 */
public class StandaloneDeploymentFactory implements DeploymentFactory {

    private static final Logger LOGGER = Logger.getLogger(StandaloneDeploymentFactory.class.getName());
    
    public static final String URI_PREFIX = "openesb-standalone-deployer:"; // NOI18N
    private static final String DISCONNECTED_URI = "openesb-standalone-deployer:http://localhost:8080&"; // NOI18N
    
    private static DeploymentFactory instance;
    
    /**
     * Mapping of a server installation directory to a deployment factory
     */
    private static Map<InstanceProperties, DeploymentFactory> FACTORIES_CACHE = new WeakHashMap<InstanceProperties, DeploymentFactory>();
    
    public static synchronized DeploymentFactory create() {
        if (instance == null) {
            instance = new StandaloneDeploymentFactory();
            DeploymentFactoryManager.getInstance().registerDeploymentFactory(instance);
        }
        return instance;
    }
    
    @Override
    public boolean handlesURI(String uri) {
        return uri != null && uri.startsWith(URI_PREFIX);
    }

    @Override
    public DeploymentManager getDeploymentManager(String uri, String uname, String passwd) throws DeploymentManagerCreationException {
        if (!handlesURI(uri)) {
            throw new DeploymentManagerCreationException(NbBundle.getMessage(StandaloneDeploymentFactory.class, "MSG_INVALID_URI", uri)); // NOI18N
        }

        return new StandaloneDeploymentManager(uri);
    }

    @Override
    public DeploymentManager getDisconnectedDeploymentManager(String uri) throws DeploymentManagerCreationException {
        if (!handlesURI(uri)) {
            throw new DeploymentManagerCreationException(NbBundle.getMessage(StandaloneDeploymentFactory.class, "MSG_INVALID_URI", uri)); // NOI18N
        }

        return new StandaloneDeploymentManager(uri);
    }
    
    @Override
    public String getDisplayName() {
        return NbBundle.getMessage(StandaloneDeploymentFactory.class, "LBL_StandaloneFactory"); // NOI18N
    }

    @Override
    public String getProductVersion() {
        return NbBundle.getMessage(StandaloneDeploymentFactory.class, "LBL_StandaloneFactoryVersion"); // NOI18N
    }
    
}
